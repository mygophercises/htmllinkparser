package parser

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/mygophercises/htmllinkparser/test_utils"
)

func TestParseLinkHtmlEx1(t *testing.T) {
	links, err := ParseForLinks(test_utils.HtmlEx1)
	if err != nil {
		t.Errorf(err.Error())
	}
	LinkOne := Link{
		Href: "/other-page",
		Text: "A link to another page",
	}

	expected := []Link{LinkOne}
	assert.EqualValues(t, expected, links, "Should deep equal expected")
}

func TestParseLinkHtmlEx2(t *testing.T) {
	links, err := ParseForLinks(test_utils.HtmlEx2)
	if err != nil {
		t.Errorf(err.Error())
	}

	LinkOne := Link{
		Href: "https://www.twitter.com/joncalhoun",
		Text: "Check me out on twitter",
	}
	LinkTwo := Link{
		Href: "https://github.com/gophercises",
		Text: "Gophercises is on Github !",
	}

	expected := []Link{LinkOne, LinkTwo}
	assert.EqualValues(t, expected, links, "Should deep equal expected")
}

func TestParseLinkHtmlEx3(t *testing.T) {
	links, err := ParseForLinks(test_utils.HtmlEx3)
	if err != nil {
		t.Errorf(err.Error())
	}

	LinkOne := Link{
		Href: "#",
		Text: "Login",
	}
	LinkTwo := Link{
		Href: "/lost",
		Text: "Lost? Need help?",
	}

	LinkThree := Link{
		Href: "https://twitter.com/marcusolsson",
		Text: "@marcusolsson",
	}

	expected := []Link{LinkOne, LinkTwo, LinkThree}
	assert.EqualValues(t, expected, links, "Should deep equal expected")
}

func TestParseLinkHtmlEx4(t *testing.T) {
	links, err := ParseForLinks(test_utils.HtmlEx4)
	if err != nil {
		t.Errorf(err.Error())
	}
	LinkOne := Link{
		Href: "/dog-cat",
		Text: "dog cat",
	}

	expected := []Link{LinkOne}
	assert.EqualValues(t, expected, links, "Should deep equal expected")
}
