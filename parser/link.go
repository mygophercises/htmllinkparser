package parser

import (
	"strings"

	"golang.org/x/net/html"
)

type Link struct {
	Href string
	Text string
}

func ParseForLinks(htmlString string) ([]Link, error) {
	links := []Link{}
	doc, err := html.Parse(strings.NewReader(htmlString))
	if err != nil {
		return nil, err
	}

	links = pageLinks(links, doc)

	return links, nil
}

func pageLinks(links []Link, node *html.Node) []Link {
	if node.Type == html.ElementNode && node.Data == "a" {
		for _, attr := range node.Attr {
			if attr.Key == "href" {
				text := []string{}
				links = append(links, Link{
					Href: attr.Val,
					Text: strings.TrimSpace(strings.Join(linkText(text, node), " ")),
				})
			}
		}
	}
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		links = pageLinks(links, c)
	}
	return links
}

func linkText(text []string, node *html.Node) []string {
	if node.Type == html.TextNode {
		text = append(text, strings.TrimSpace(node.Data))
	}
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		text = linkText(text, c)
	}
	return text
}
