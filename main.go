package main

import (
	"flag"
	"io/ioutil"
	"log"
	"net/http"

	"gitlab.com/mygophercises/htmllinkparser/parser"
)

func main() {
	url := flag.String("url", "http://www.google.com", "the html page to be parsed")
	flag.Parse()
	r, err := http.Get(*url)
	if err != nil {
		panic(err)
	}
	pageByte, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	log.Print(parser.ParseForLinks(string(pageByte)))
}
