module gitlab.com/mygophercises/htmllinkparser

go 1.16

require (
	github.com/stretchr/testify v1.7.0
	golang.org/x/net v0.0.0-20210525063256-abc453219eb5
)
